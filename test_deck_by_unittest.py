	def test_Card_init(self):
		"""Testing Card __repr__"""
		self.assertEqual(self.card.suit, "Spades")
		self.assertEqual(self.card.value, "A")

	def test_Card_repr(self):
		"""Testing Card __init__"""
		self.assertEqual(repr(self.card), "A of Spades")

class TestDeckMethods(unittest.TestCase):

	def setUp(self):
		self.test_deck = Deck()

	def test_init(self):
		self.assertTrue(isinstance(self.test_deck.cards, list))
		self.assertEqual(len(self.test_deck.cards), 52)

	def test_repr(self):
		self.assertEqual(repr(self.test_deck), "Deck of 52 cards.")

	def test_count(self):
		self.assertEqual(self.test_deck.count(), 52)
		self.test_deck.cards.pop()
		self.assertEqual(self.test_deck.count(), 51)

	def test_shuffle_test(self):
		test = self.test_deck.cards[:]
		self.assertEqual(self.test_deck.cards, test)
		self.test_deck.shuffle()
		self.assertNotEqual(self.test_deck, test)
		self.assertEqual(self.test_deck.count(), 52)
		with self.assertRaises(ValueError):
			self.test_deck._deal(1)
			self.test_deck.shuffle()

	def test__deal(self):
		cards = self.test_deck._deal(7)
		self.assertEqual(len(cards), 7)
		self.assertEqual(self.test_deck.count(), 45)
		with self.assertRaises(ValueError):
			self.test_deck._deal(100)

	def test_deal_card(self):
		card = self.test_deck.deal_card()
		self.assertEqual(self.test_deck.count(), 51)
		self.assertTrue(isinstance(card, Card))

	def test_deal_hand_test(self):
		hand = self.test_deck.deal_hand(5)
		self.assertEqual(len(hand), 5)
		self.assertEqual(self.test_deck.count(), 47)


if __name__ == "__main__":
	unittest.main()