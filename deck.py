from random import shuffle

class Card:
	def __init__(self, suit, value):
		self.suit = suit
		self.value = value

	def __repr__(self):
		return f"{self.value} of {self.suit}"

class Deck:
	def __init__(self):
		values = ("A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K")
		suits = ("Hearts", "Diamonds", "Clubs", "Spades")
		self.cards = [Card(s, v) for s in suits for v in values]

	def __repr__(self):
		return f"Deck of {self.count()} cards."

	def count(self):
		return len(self.cards)

	def _deal(self, num):
		cards = []
		if self.count() >= num:
			if num == 1:
				return self.cards.pop()
			elif num > 1:
				self.cards = self.cards[:-num]
				return self.cards[-num:]
		else:
			raise ValueError("All cards have been dealt")
			self.cards = self.cards[:-(self.cards.count())]
			return self.cards[-(self.cards.count()):]

	def shuffle(self):
		if self.count() == 52:
			return shuffle(self.cards)
		else:
			raise ValueError("Only full decks can be shuffled")

	def deal_card(self):
		return self._deal(1)

	def deal_hand(self, hand):
		return self._deal(hand)